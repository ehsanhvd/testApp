package di;

import com.example.hasanvand.testapp.MainPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Hasanvand on 1/31/2018.
 */
@Module
public class AppModule {

    @Provides
    MainPresenter getMainPresenter(){
        return new MainPresenter();
    }
}
