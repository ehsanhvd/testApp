package com.example.hasanvand.testapp;

import android.view.View;
import android.widget.Toast;

/**
 * Created by Hasanvand on 1/27/2018.
 */

public class MainPresenter extends BasePresenter implements MainPresenterInterface {

    @Override
    public String getText() {
        return "injection ok";
    }

    public void onSaveClick(User user) {
        getView().feedBack(user.getFirstName());
    }

    @Override
    public MainActivity getView() {
        return (MainActivity) super.getView();
    }
}
